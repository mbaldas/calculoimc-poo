import java.util.Scanner;

public class Principal {
	
	public static void main (String[] args) {
		
		CalculaIMC imc = new CalculaIMC();
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Digite sua altura");
		imc.altura = teclado.nextDouble();
		
		System.out.println("Digite seu peso");
		imc.peso = teclado.nextDouble();
		
		teclado.close();
		
		System.out.println ("O seu imc é:" + imc.calculaimc());
		
		if (imc.calculaimc() < 18.5) {
			System.out.println("Magreza");
		}
		else if (imc.calculaimc() < 25) {
			System.out.println("Saudável");
		}
		else if (imc.calculaimc() < 30) {
			System.out.println("Sobrepeso");
		}
		else if (imc.calculaimc() < 35) {
			System.out.println("Obesidade grau 1");
		}
		else if (imc.calculaimc() < 40) {
			System.out.println("Obesidade grau 2 (Severa)");
		}
		else{
			System.out.println("Obesidade grau 3 (Mórbida)");
		}
		
	}	
}
